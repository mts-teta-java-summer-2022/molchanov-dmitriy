package com.mts.teta.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.mts.teta.api.dto.CourseDto;
import com.mts.teta.entity.CourseEntity;
import com.mts.teta.utils.TestUtils;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CourseMapperTest {

  @InjectMocks
  private CourseMapperImpl courseMapper;

  @Test
  public void toEntity_testMapper_correctEntityReturned() {
    CourseDto courseDto = TestUtils.getCourseDto();
    CourseEntity courseEntity = courseMapper.toEntity(courseDto);

    assertEquals(courseEntity.getAuthor(), courseDto.getAuthor());
    assertEquals(courseEntity.getTitle(), courseDto.getTitle());
  }

  @Test
  public void toDto_testMapper_correctDtoReturned() {
    CourseEntity courseEntity = TestUtils.getCourseEntity();
    CourseDto courseDto = courseMapper.toDto(courseEntity);

    assertEquals(courseEntity.getAuthor(), courseDto.getAuthor());
    assertEquals(courseEntity.getTitle(), courseDto.getTitle());
  }

  @Test
  public void toDtos_testMapper_correctDtosReturned() {
    CourseEntity courseEntity = TestUtils.getCourseEntity();
    CourseDto courseDto = courseMapper.toDtos(List.of(courseEntity)).get(0);

    assertEquals(courseEntity.getAuthor(), courseDto.getAuthor());
    assertEquals(courseEntity.getTitle(), courseDto.getTitle());
  }

}
