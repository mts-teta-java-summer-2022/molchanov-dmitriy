package com.mts.teta.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mts.teta.api.dto.CourseDto;
import com.mts.teta.entity.CourseEntity;
import com.mts.teta.repository.CourseRepository;
import com.mts.teta.utils.TestUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class CourseServiceTest {

  @Autowired
  private CourseService courseService;
  @Autowired
  private CourseRepository courseRepository;

  @Test
  public void getCourseById_testMethod_correctEntityReturned() {
    assertNotNull(courseService.getCourseById(courseRepository.save(TestUtils.getCourseEntity()).getId()));
  }

  @Test
  public void getCoursesByPrefix_findByPrefixNew_oneEntityReturned() {
    CourseEntity course = TestUtils.getCourseEntity();
    CourseEntity course2 = TestUtils.getCourseEntity();
    CourseEntity course3 = TestUtils.getCourseEntity();
    courseRepository.save(course);
    course2.setTitle("new title");
    courseRepository.save(course2);
    course3.setTitle("super new title");
    courseRepository.save(course3);
    List<CourseDto> dtoList = courseService.getCoursesByPrefix("new");
    assertEquals(3, courseRepository.findAll().size());
    assertEquals(1, dtoList.size());
  }

  @Test
  public void updateCourse_saveNewDto_updatesCourseReturned() {
    UUID id = courseRepository.save(TestUtils.getCourseEntity()).getId();
    CourseDto newCourse = new CourseDto();
    newCourse.setAuthor("new_author");
    newCourse.setTitle("new_title");
    courseService.updateCourse(id, newCourse);
    CourseDto courseDto = courseService.getCourseById(id);
    assertNotEquals(courseDto.getAuthor(), TestUtils.getCourseEntity().getAuthor());
  }

  @Test
  public void deleteCourse_deleteById_statusOkReturned() {
    UUID id = courseRepository.save(TestUtils.getCourseEntity()).getId();
    assertEquals(1, courseRepository.findAll().size());
    courseService.deleteCourse(id);
    assertThrows(ObjectOptimisticLockingFailureException.class, () -> courseRepository.findAll());
  }


  @SneakyThrows
  @Test
  public void test() {
    String json = "{\n"
        + "    \"entity\" : {\n"
        + "        \"id\" : \"9042af07-1f05-11ed-b3d6-a267c491ecf2\",\n"
        + "        \"author\" : \"aaa\",\n"
        + "        \"title\" : \"bbb\"\n"
        + "    },\n"
        + "    \"entities\" : [\n"
        + "        {\n"
        + "            \"id\" : \"9042af07-1f05-11ed-b3d6-a267c491ecf2\",\n"
        + "            \"author\" : \"aaa2\",\n"
        + "            \"title\" : \"bbb2\"\n"
        + "        }\n"
        + "    ]\n"
        + "}";

    Map<String,Object> result =
        new ObjectMapper().readValue(json, HashMap.class);

    CourseEntity map = new ObjectMapper().convertValue(result.get("entity"), CourseEntity.class);
    List<CourseEntity> maps = new ObjectMapper().convertValue(result.get("entities"),new TypeReference<>(){});

    System.out.println(result);
    System.out.println(map.getAuthor());
    System.out.println(maps.get(0).getAuthor());

    String json2 = "{\"variables\": {\n"
        + "\"documents\": [\"string\", \"string\"] }}";
    result =
        new ObjectMapper().readValue(json2, HashMap.class);
    HashMap aa = (HashMap) result.get("variables");
    ArrayList<String> ss = (ArrayList<String>) aa.get("documents");
    System.out.println(ss);
//    System.out.println(a);
  }
}