package com.mts.teta.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import com.mts.teta.utils.TitleCase;
import com.mts.teta.utils.TitleCaseEnum;
import com.mts.teta.utils.TitleCaseValidator;
import javax.validation.ConstraintValidatorContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TitleCaseTest {

  @Mock
  private TitleCase titleCase;

  @Mock
  private ConstraintValidatorContext context;

  private final TitleCaseValidator titleCaseValidator = new TitleCaseValidator();

  @Test
  public void titleCase_testRuLang_correctBooleanReturned() {
    when(titleCase.lang()).thenReturn(TitleCaseEnum.EN);
    titleCaseValidator.initialize(titleCase);

    assertTrue(titleCaseValidator.isValid("Test", context));
    assertTrue(titleCaseValidator.isValid("Test Test", context));
    assertTrue(titleCaseValidator.isValid("Test,,, Test:\"'", context));

    assertFalse(titleCaseValidator.isValid("test", context));
    assertFalse(titleCaseValidator.isValid("TestTest", context));
    assertFalse(titleCaseValidator.isValid("Test test", context));
    assertFalse(titleCaseValidator.isValid("Test Test ", context));
    assertFalse(titleCaseValidator.isValid(" Test test", context));
    assertFalse(titleCaseValidator.isValid("Test Test!", context));
    assertFalse(titleCaseValidator.isValid("8Test", context));
    assertFalse(titleCaseValidator.isValid("Test Test 666", context));
    assertFalse(titleCaseValidator.isValid("Test тест", context));
    assertFalse(titleCaseValidator.isValid("Test Тест", context));
    assertFalse(titleCaseValidator.isValid("Test  Test", context));
    assertFalse(titleCaseValidator.isValid("Тест тест тест", context));
    assertFalse(titleCaseValidator.isValid(",Test Test", context));
  }

  @Test
  public void titleCase_testEnLang_correctBooleanReturned() {
    when(titleCase.lang()).thenReturn(TitleCaseEnum.RU);
    titleCaseValidator.initialize(titleCase);

    assertTrue(titleCaseValidator.isValid("Тест тест тест", context));
    assertTrue(titleCaseValidator.isValid("Тест тест:\"',,,,,,", context));
    assertTrue(titleCaseValidator.isValid("Тест", context));

    assertFalse(titleCaseValidator.isValid("test", context));
    assertFalse(titleCaseValidator.isValid("Test", context));
    assertFalse(titleCaseValidator.isValid("Test тест", context));
    assertFalse(titleCaseValidator.isValid("ТестТестТестТест", context));
    assertFalse(titleCaseValidator.isValid("Test Тест", context));
    assertFalse(titleCaseValidator.isValid("Тест  тест тест", context));
    assertFalse(titleCaseValidator.isValid("Тест ттт8", context));
    assertFalse(titleCaseValidator.isValid("Тест тест Тест", context));
    assertFalse(titleCaseValidator.isValid("Тест тест ", context));
    assertFalse(titleCaseValidator.isValid(" Тест тест", context));
    assertFalse(titleCaseValidator.isValid("9Тест тест ", context));
    assertFalse(titleCaseValidator.isValid(",Тест тест ", context));
  }

  @Test
  public void titleCase_testAnyLang_correctBooleanReturned() {
    when(titleCase.lang()).thenReturn(TitleCaseEnum.valueOf("ANY"));
    titleCaseValidator.initialize(titleCase);

    assertTrue(titleCaseValidator.isValid("test", context));
    assertTrue(titleCaseValidator.isValid("Test", context));
    assertTrue(titleCaseValidator.isValid("TestTest", context));
    assertTrue(titleCaseValidator.isValid("Test test", context));
    assertTrue(titleCaseValidator.isValid("Test Test", context));
    assertTrue(titleCaseValidator.isValid("Test Test ", context));
    assertTrue(titleCaseValidator.isValid(" Test test", context));
    assertTrue(titleCaseValidator.isValid("Test,,, Test:\"'", context));
    assertTrue(titleCaseValidator.isValid("Test Test!", context));
    assertTrue(titleCaseValidator.isValid("8Test", context));
    assertTrue(titleCaseValidator.isValid("Test Test 666", context));
    assertTrue(titleCaseValidator.isValid("Test тест", context));
    assertTrue(titleCaseValidator.isValid("Test Тест", context));
    assertTrue(titleCaseValidator.isValid("Test  Test", context));
    assertTrue(titleCaseValidator.isValid("Тест тест тест", context));
    assertTrue(titleCaseValidator.isValid("Тест  тест тест", context));
    assertTrue(titleCaseValidator.isValid("Тест тест:\"',,,,,,", context));
    assertTrue(titleCaseValidator.isValid(",Тест тест", context));
    assertTrue(titleCaseValidator.isValid("Тест ттт8", context));
  }
}
