package com.mts.teta.utils;

import com.mts.teta.api.dto.CourseDto;
import com.mts.teta.entity.CourseEntity;

public class TestUtils {

  public static CourseDto getCourseDto() {
    CourseDto courseDto = new CourseDto();
    courseDto.setTitle("test_title");
    courseDto.setAuthor("test_author");
    return courseDto;
  }

  public static CourseEntity getCourseEntity() {
    CourseEntity courseEntity = new CourseEntity();
    courseEntity.setAuthor("test_author");
    courseEntity.setTitle("test_title");
    return courseEntity;
  }

}