package com.mts.teta.utils;

import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TitleCaseValidator implements ConstraintValidator<TitleCase, String> {

  private TitleCaseEnum lang;

  @Override
  public void initialize(TitleCase titleCase) {
    this.lang = titleCase.lang();
  }

  @Override
  public boolean isValid(String title, ConstraintValidatorContext constraintValidatorContext) {
      switch (lang) {
        case EN: return checkTitle(title, "^[A-Z][a-z,:\"']+( [A-Z]{1}[a-z,:\"']+)*$");
        case RU: return checkTitle(title, "^[А-Я][а-я,:\"']+( [а-я,:\"']+)*$");
        case ANY: return true;
        default: return false;
    }
  }


  private boolean checkTitle(String title, String regex) {
    return Pattern.compile(regex).matcher(title).matches();
  }

}