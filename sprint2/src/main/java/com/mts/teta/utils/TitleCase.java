package com.mts.teta.utils;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = TitleCaseValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
public @interface TitleCase {

  TitleCaseEnum lang() default TitleCaseEnum.ANY;

  String message() default "Invalid title";

  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};

}