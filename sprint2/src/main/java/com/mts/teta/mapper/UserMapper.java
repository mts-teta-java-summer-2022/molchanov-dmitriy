package com.mts.teta.mapper;

import com.mts.teta.api.dto.UserDto;
import com.mts.teta.entity.UserEntity;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

  UserEntity toEntity(UserDto userDto);

  UserDto toDto(UserEntity userEntity);

  List<UserDto> toDtos(List<UserEntity> userEntities);

}