package com.mts.teta.mapper;

import com.mts.teta.api.dto.LessonDto;
import com.mts.teta.entity.LessonEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface LessonMapper {

  @Mapping(source = "lessonDto.courseId", target = "course.id")
  LessonEntity toEntity(LessonDto lessonDto);

  @Mapping(source = "lessonEntity.course.id", target = "courseId")
  LessonDto toDto(LessonEntity lessonEntity);

  List<LessonDto> toDtos(List<LessonEntity> lessonEntities);

}