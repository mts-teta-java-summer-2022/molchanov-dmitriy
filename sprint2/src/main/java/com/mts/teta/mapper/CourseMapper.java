package com.mts.teta.mapper;

import com.mts.teta.api.dto.CourseDto;
import com.mts.teta.entity.CourseEntity;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CourseMapper {

  CourseEntity toEntity(CourseDto courseDto);

  CourseDto toDto(CourseEntity courseEntity);

  List<CourseDto> toDtos(List<CourseEntity> courseEntities);

}
