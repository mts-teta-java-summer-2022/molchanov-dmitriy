package com.mts.teta.repository;

import com.mts.teta.entity.LessonEntity;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LessonRepository extends JpaRepository<LessonEntity, UUID> {

  void deleteAllByCourse_Id(UUID courseId);

  List<LessonEntity> findAllByCourse_Id(UUID courseId);

}