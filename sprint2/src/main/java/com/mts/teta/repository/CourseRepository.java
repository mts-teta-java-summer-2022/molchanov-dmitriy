package com.mts.teta.repository;

import com.mts.teta.entity.CourseEntity;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<CourseEntity, UUID> {

  @Query(value = "SELECT * from COURSE as c WHERE c.title LIKE ?1", nativeQuery = true)
  List<CourseEntity> findAllByCoursePrefix(String prefix);

}