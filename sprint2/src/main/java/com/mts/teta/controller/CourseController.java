package com.mts.teta.controller;

import com.mts.teta.api.CourseApi;
import com.mts.teta.api.dto.CourseDto;
import com.mts.teta.service.CourseService;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CourseController implements CourseApi {

  private final CourseService courseService;

  @Override
  public CourseDto getCourseById(UUID id) {
    return courseService.getCourseById(id);
  }

  @Override
  public List<CourseDto> getCourses(String prefix) {
    return courseService.getCoursesByPrefix(prefix);
  }

  @Override
  public void saveCourse(CourseDto courseDto) {
    courseService.saveCourse(courseDto);
  }

  @Override
  public void updateCourse(UUID id, CourseDto courseDto) {
    courseService.updateCourse(id, courseDto);
  }

  @Override
  public void deleteCourse(UUID id) {
    courseService.deleteCourse(id);
  }

  @Override
  public void addUserToCourse(UUID id, UUID userId) {
    courseService.addUserToCourse(id, userId);
  }
}
