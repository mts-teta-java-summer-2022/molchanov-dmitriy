package com.mts.teta.controller;

import com.mts.teta.api.UserApi;
import com.mts.teta.api.dto.UserDto;
import com.mts.teta.service.UserService;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

  private final UserService userService;

  @Override
  public UserDto getUserById(UUID id) {
    return userService.findById(id);
  }

  @Override
  public List<UserDto> getUsers() {
    return userService.findAll();
  }

  @Override
  public UUID saveUser(UserDto userDto) {
    return userService.save(userDto);
  }

  @Override
  public void deleteUser(UUID id) {
    userService.delete(id);
  }
}