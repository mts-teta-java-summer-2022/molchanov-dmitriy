package com.mts.teta.controller;

import com.mts.teta.api.LessonApi;
import com.mts.teta.api.dto.LessonDto;
import com.mts.teta.service.LessonService;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class LessonController implements LessonApi {

  private final LessonService lessonService;

  @Override
  public LessonDto getLessonById(UUID id) {
    return lessonService.findById(id);
  }

  @Override
  public List<LessonDto> getLessonsByCourseId(UUID id) {
    return lessonService.findAllByCourseId(id);
  }

  @Override
  public UUID saveLesson(LessonDto lessonDto) {
    return lessonService.save(lessonDto);
  }

  @Override
  public void deleteLessonByCourseId(UUID id) {
    lessonService.deleteByCourseId(id);
  }

}