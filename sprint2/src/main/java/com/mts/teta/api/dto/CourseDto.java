package com.mts.teta.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mts.teta.utils.TitleCase;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Set;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@Validated
@NoArgsConstructor
public class CourseDto {

  @JsonProperty("id")
  @Schema(accessMode = Schema.AccessMode.READ_ONLY)
  private UUID id;
  @Valid
  @JsonProperty("author")
  @NotBlank(message = "Course author has to be filled")
  private String author;
  @Valid
  @TitleCase()
  @JsonProperty("title")
  @NotBlank(message = "Course title has to be filled")
  private String title;

  @JsonProperty("users")
  @Schema(accessMode = Schema.AccessMode.READ_ONLY)
  private Set<UserDto> users;

}