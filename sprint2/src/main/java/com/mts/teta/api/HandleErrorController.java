package com.mts.teta.api;

import com.mts.teta.api.dto.ApiErrorDto;
import java.time.OffsetDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class HandleErrorController {

  @ExceptionHandler(Exception.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public ApiErrorDto noSuchElementExceptionHandler(Exception ex) {
    return new ApiErrorDto(OffsetDateTime.now(), ex.getMessage());
  }

}