package com.mts.teta.api;

import com.mts.teta.api.dto.UserDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Validated
@Tag(name = "Users")
public interface UserApi {

  @Operation(summary = "get user by id")
  @ResponseStatus(HttpStatus.OK)
  @GetMapping("/api/v1/users/{id}")
  UserDto getUserById(@PathVariable UUID id);

  @Operation(summary = "get all users")
  @ResponseStatus(HttpStatus.OK)
  @GetMapping("/api/v1/users")
  List<UserDto> getUsers();

  @Operation(summary = "save user")
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping("/api/v1/users")
  UUID saveUser(@Valid @RequestBody UserDto userDto);

  @Operation(summary = "delete user by id")
  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping("/api/v1/users/{id}")
  void deleteUser(@PathVariable UUID id);

}