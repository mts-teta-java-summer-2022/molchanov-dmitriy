package com.mts.teta.api.dto;

import java.time.OffsetDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ApiErrorDto {

  private OffsetDateTime dateOccurred;
  private String message;

}