package com.mts.teta.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@Validated
@NoArgsConstructor
public class LessonDto {

  @JsonProperty("id")
  @Schema(accessMode = Schema.AccessMode.READ_ONLY)
  private UUID id;
  @JsonProperty("title")
  private String title;
  @JsonProperty("text")
  private String text;
  @JsonProperty("courseId")
  private UUID courseId;

}