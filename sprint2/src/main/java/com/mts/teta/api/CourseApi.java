package com.mts.teta.api;

import com.mts.teta.api.dto.CourseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@Validated
@Tag(name = "Courses")
public interface CourseApi {

  @Operation(summary = "get course by id")
  @ResponseStatus(HttpStatus.OK)
  @GetMapping("/api/v1/courses/{id}")
  CourseDto getCourseById(@PathVariable UUID id);

  @Operation(summary = "get all courses")
  @ResponseStatus(HttpStatus.OK)
  @GetMapping("/api/v1/courses")
  List<CourseDto> getCourses(@RequestParam(name = "prefix", required = false) String prefix);

  @Operation(summary = "save course")
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping("/api/v1/courses")
  void saveCourse(@Valid @RequestBody CourseDto courseDto);

  @Operation(summary = "update course")
  @ResponseStatus(HttpStatus.OK)
  @PutMapping("/api/v1/courses/{id}")
  void updateCourse(@PathVariable UUID id, @Valid @RequestBody CourseDto courseDto);

  @Operation(summary = "delete course by id")
  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping("/api/v1/courses/{id}")
  void deleteCourse(@PathVariable UUID id);

  @Operation(summary = "add user to course by course id")
  @ResponseStatus(HttpStatus.CREATED)
  @PatchMapping("/api/v1/courses/{id}")
  void addUserToCourse(@Valid @NotNull @PathVariable UUID id, @Valid @NotNull @RequestParam UUID userId);

}