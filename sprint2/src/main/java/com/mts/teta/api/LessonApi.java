package com.mts.teta.api;

import com.mts.teta.api.dto.LessonDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Validated
@Tag(name = "Lessons")
public interface LessonApi {

  @Operation(summary = "get lesson by id")
  @ResponseStatus(HttpStatus.OK)
  @GetMapping("/api/v1/courses/lessons/{id}")
  LessonDto getLessonById(@PathVariable UUID id);

  @Operation(summary = "get all lessons by course")
  @ResponseStatus(HttpStatus.OK)
  @GetMapping("/api/v1/courses/{id}/lessons")
  List<LessonDto> getLessonsByCourseId(@PathVariable UUID id);

  @Operation(summary = "save lesson")
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping("/api/v1/courses/lessons")
  UUID saveLesson(@Valid @RequestBody LessonDto lessonDto);

  @Operation(summary = "delete all lessons by course")
  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping("/api/v1/courses/{id}/lessons")
  void deleteLessonByCourseId(@PathVariable UUID id);

}