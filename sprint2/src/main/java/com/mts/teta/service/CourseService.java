package com.mts.teta.service;

import com.mts.teta.api.dto.CourseDto;
import com.mts.teta.entity.CourseEntity;
import com.mts.teta.entity.UserEntity;
import com.mts.teta.mapper.CourseMapper;
import com.mts.teta.repository.CourseRepository;
import com.mts.teta.repository.UserRepository;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CourseService {

  private final CourseRepository courseRepository;
  private final CourseMapper courseMapper;
  private final UserRepository userRepository;

  public CourseDto getCourseById(UUID id) {
    return courseMapper.toDto(courseRepository.findById(id).orElseThrow());
  }

  public List<CourseDto> getCoursesByPrefix(String prefix) {
    try {
      return courseMapper.toDtos(courseRepository.findAllByCoursePrefix(String.format("%s%%",prefix)));
    } catch (Exception e) {
      return Collections.emptyList();
    }
  }

  public void saveCourse(CourseDto courseDto) {
    courseRepository.save(courseMapper.toEntity(courseDto));
  }

  public void updateCourse(UUID id, CourseDto courseDto) {
    CourseEntity course = courseRepository.findById(id).orElseThrow();
    course.setAuthor(courseDto.getAuthor());
    course.setTitle(courseDto.getTitle());
    courseRepository.save(course);
  }

  public void deleteCourse(UUID id) {
    getCourseById(id);
    courseRepository.deleteById(id);
  }

  public void addUserToCourse(UUID courseId, UUID userId) {
    UserEntity user = userRepository.findById(userId).orElseThrow();
    CourseEntity course = courseRepository.findById(courseId).orElseThrow();
    user.getCourses().add(course);
    course.getUsers().add(user);
    courseRepository.save(course);
  }

}