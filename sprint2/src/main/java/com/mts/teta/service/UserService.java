package com.mts.teta.service;

import com.mts.teta.api.dto.UserDto;
import com.mts.teta.entity.UserEntity;
import com.mts.teta.mapper.UserMapper;
import com.mts.teta.repository.UserRepository;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService {

  private final UserRepository userRepository;
  private final UserMapper userMapper;

  public UserDto findById(UUID id) {
    return userMapper.toDto(userRepository.findById(id).orElseThrow());
  }

  public List<UserDto> findAll() {
    return userMapper.toDtos(userRepository.findAll());
  }

  public UUID save(UserDto user) {
    return userRepository.save(userMapper.toEntity(user)).getId();
  }

  public void delete(UUID id) {
    UserEntity user = userRepository.findById(id).orElseThrow();
    userRepository.delete(user);
  }

}