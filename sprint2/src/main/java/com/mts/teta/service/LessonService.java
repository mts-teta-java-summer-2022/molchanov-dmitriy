package com.mts.teta.service;

import com.mts.teta.api.dto.LessonDto;
import com.mts.teta.entity.CourseEntity;
import com.mts.teta.mapper.LessonMapper;
import com.mts.teta.repository.CourseRepository;
import com.mts.teta.repository.LessonRepository;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LessonService {

  private final CourseRepository courseRepository;
  private final LessonRepository repository;
  private final LessonMapper mapper;

  public LessonDto findById(UUID id) {
    return mapper.toDto(repository.findById(id).orElseThrow());
  }

  public List<LessonDto> findAllByCourseId(UUID courseId) {
    return mapper.toDtos(repository.findAllByCourse_Id(courseId));
  }

  public UUID save(LessonDto lesson) {
    return repository.save(mapper.toEntity(lesson)).getId();
  }

  public void deleteByCourseId(UUID courseId) {
    repository.deleteAllByCourse_Id(courseId);
  }

}