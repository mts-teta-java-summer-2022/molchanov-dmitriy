openapi: 3.0.1
info:
  title: OpenAPI definition
  version: v0
servers:
  - url: http://localhost:8080
    description: Generated server url
paths:
  /api/v1/courses/{id}:
    get:
      tags:
        - Courses
      summary: get course by id
      operationId: getCourseById
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: OK
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/CourseDto'
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
    put:
      tags:
        - Courses
      summary: update course
      operationId: updateCourse
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CourseDto'
        required: true
      responses:
        '200':
          description: OK
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
    delete:
      tags:
        - Courses
      summary: delete course by id
      operationId: deleteCourse
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: OK
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
    patch:
      tags:
        - Courses
      summary: add user to course by course id
      operationId: addUserToCourse
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
        - name: userId
          in: query
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '201':
          description: Created
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
  /api/v1/users:
    get:
      tags:
        - Users
      summary: get all users
      operationId: getUsers
      responses:
        '200':
          description: OK
          content:
            '*/*':
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/UserDto'
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
    post:
      tags:
        - Users
      summary: save user
      operationId: saveUser
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserDto'
        required: true
      responses:
        '201':
          description: Created
          content:
            '*/*':
              schema:
                type: string
                format: uuid
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
  /api/v1/courses:
    get:
      tags:
        - Courses
      summary: get all courses
      operationId: getCourses
      parameters:
        - name: prefix
          in: query
          required: false
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            '*/*':
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/CourseDto'
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
    post:
      tags:
        - Courses
      summary: save course
      operationId: saveCourse
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CourseDto'
        required: true
      responses:
        '201':
          description: Created
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
  /api/v1/courses/lessons:
    post:
      tags:
        - Lessons
      summary: save lesson
      operationId: saveLesson
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LessonDto'
        required: true
      responses:
        '201':
          description: Created
          content:
            '*/*':
              schema:
                type: string
                format: uuid
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
  /api/v1/users/{id}:
    get:
      tags:
        - Users
      summary: get user by id
      operationId: getUserById
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: OK
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/UserDto'
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
    delete:
      tags:
        - Users
      summary: delete user by id
      operationId: deleteUser
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: OK
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
  /api/v1/courses/{id}/lessons:
    get:
      tags:
        - Lessons
      summary: get all lessons by course
      operationId: getLessonsByCourseId
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: OK
          content:
            '*/*':
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/LessonDto'
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
    delete:
      tags:
        - Lessons
      summary: delete all lessons by course
      operationId: deleteLessonByCourseId
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: OK
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
  /api/v1/courses/lessons/{id}:
    get:
      tags:
        - Lessons
      summary: get lesson by id
      operationId: getLessonById
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: OK
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/LessonDto'
        '404':
          description: Not Found
          content:
            '*/*':
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
components:
  schemas:
    ApiErrorDto:
      type: object
      properties:
        dateOccurred:
          type: string
          format: date-time
        message:
          type: string
    CourseDto:
      required:
        - author
        - title
      type: object
      properties:
        id:
          type: string
          format: uuid
          readOnly: true
        author:
          type: string
        title:
          type: string
        users:
          uniqueItems: true
          type: array
          readOnly: true
          items:
            $ref: '#/components/schemas/UserDto'
    UserDto:
      type: object
      properties:
        id:
          type: string
          format: uuid
          readOnly: true
        username:
          type: string
      readOnly: true
    LessonDto:
      type: object
      properties:
        id:
          type: string
          format: uuid
          readOnly: true
        title:
          type: string
        text:
          type: string
        courseId:
          type: string
          format: uuid
