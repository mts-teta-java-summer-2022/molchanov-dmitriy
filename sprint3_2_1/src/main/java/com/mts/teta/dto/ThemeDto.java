package com.mts.teta.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@Validated
@NoArgsConstructor
@AllArgsConstructor
public class ThemeDto {

  @JsonProperty("title")
  private String title;
  @JsonProperty("description")
  private String description;
  @JsonProperty("createDate")
  private LocalDate createDate;
  @JsonProperty("createBy")
  private String createBy;
  @JsonProperty("lastUpdateDate")
  private LocalDate lastUpdateDate;
  @JsonProperty("updateBy")
  private String updateBy;
  @JsonProperty("deleteDate")
  private LocalDate deleteDate;
  @JsonProperty("deleteBy")
  private String deleteBy;
  @JsonProperty("content")
  private String content;
  @JsonProperty("tasks")
  private String tasks;

}