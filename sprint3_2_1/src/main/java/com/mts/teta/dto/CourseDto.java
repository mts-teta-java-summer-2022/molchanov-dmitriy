package com.mts.teta.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@Validated
@NoArgsConstructor
public class CourseDto {

  @JsonProperty("title")
  private String title;
  @JsonProperty("description")
  private String description;
  @JsonProperty("createDate")
  private LocalDate createDate;
  @JsonProperty("createBy")
  private String createBy;
  @JsonProperty("lastUpdateDate")
  private LocalDate lastUpdateDate;
  @JsonProperty("updateBy")
  private String updateBy;
  @JsonProperty("deleteDate")
  private LocalDate deleteDate;
  @JsonProperty("deleteBy")
  private String deleteBy;
  @JsonProperty("timePassage")
  private String timePassage;
  @JsonProperty("courseScore")
  private String courseScore;
  @JsonProperty("modules")
  private String modules;
  @JsonProperty("tag")
  private String tag;
  @JsonProperty("category")
  private String category;

}