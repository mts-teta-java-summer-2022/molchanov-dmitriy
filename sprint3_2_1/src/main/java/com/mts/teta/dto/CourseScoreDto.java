package com.mts.teta.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@Validated
@NoArgsConstructor
public class CourseScoreDto {

  @JsonProperty("userId")
  String userId;
  @JsonProperty("courseId")
  String courseId;
  @JsonProperty("score")
  String score;

}