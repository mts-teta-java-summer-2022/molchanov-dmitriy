package com.mts.teta.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@Validated
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

  @JsonProperty("nickname")
  private String nickname;
  @JsonProperty("password")
  private String password;
  @JsonProperty("firstName")
  private String firstName;
  @JsonProperty("lastName")
  private String lastName;
  @JsonProperty("email")
  private String email;
  @JsonProperty("avatar")
  private String avatar;
  @JsonProperty("dateOfRegistration")
  private String dateOfRegistration;
  @JsonProperty("lastUpdateDate")
  private LocalDate lastUpdateDate;
  @JsonProperty("updateBy")
  private String updateBy;
  @JsonProperty("deleteDate")
  private LocalDate deleteDate;
  @JsonProperty("deleteBy")
  private String deleteBy;
  @JsonProperty("adminPanelAccess")
  private boolean adminPanelAccess;
  @JsonProperty("courseId")
  private String courseId;

}