package com.mts.teta.entity;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "course")
@EntityListeners(AuditingEntityListener.class)
public class CourseEntity {

  @Id
  @GeneratedValue(generator="system-uuid")
  @GenericGenerator(name="system-uuid", strategy = "uuid2")
  private UUID id;

  @Column(name = "title")
  private String title;
  @Column(name = "description")
  private String description;
  @CreatedDate
  @Column(name = "create_date")
  private LocalDate createDate;
  @Column(name = "create_by")
  @CreatedBy
  private String createBy;
  @LastModifiedDate
  @Column(name = "last_update_date")
  private LocalDate lastUpdateDate;
  @LastModifiedBy
  @Column(name = "update_by")
  private String updateBy;
  @Column(name = "delete_date")
  private LocalDate deleteDate;
  @Column(name = "delete_by")
  private String deleteBy;
  @Column(name = "time_passage")
  private String timePassage;
  @Column(name = "tag")
  private String tag;
  @Column(name = "category")
  private String category;
  @Column(name = "course_score")
  private String courseScore;

  @OneToMany(mappedBy="course", cascade = CascadeType.ALL)
  private List<ModuleEntity> modules;

  @ManyToMany
  @JoinTable(
      name = "COURSE_SCORE",
      joinColumns = @JoinColumn(name = "user_id"),
      inverseJoinColumns = @JoinColumn(name = "course_id"))
  private Set<UserEntity> users;

}