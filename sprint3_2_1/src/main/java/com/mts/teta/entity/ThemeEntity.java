package com.mts.teta.entity;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "theme")
@EntityListeners(AuditingEntityListener.class)
public class ThemeEntity {

  @Id
  @GeneratedValue(generator="system-uuid")
  @GenericGenerator(name="system-uuid", strategy = "uuid2")
  private UUID id;

  @Column(name = "title")
  private String title;
  @Column(name = "description")
  private String description;
  @CreatedDate
  @Column(name = "create_date")
  private LocalDate createDate;
  @CreatedBy
  @Column(name = "create_by")
  private String createBy;
  @LastModifiedDate
  @Column(name = "last_update_date")
  private LocalDate lastUpdateDate;
  @LastModifiedBy
  @Column(name = "update_by")
  private String updateBy;
  @Column(name = "delete_date")
  private LocalDate deleteDate;
  @Column(name = "delete_by")
  private String deleteBy;
  @Column(name = "content")
  private String content;

  @OneToMany(mappedBy = "theme", cascade = CascadeType.ALL)
  private List<TaskEntity> tasks;

  @ManyToOne
  @JoinColumn(name="module_id", nullable=false)
  private ModuleEntity module;

}