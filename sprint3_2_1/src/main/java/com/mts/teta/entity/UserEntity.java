package com.mts.teta.entity;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user")
@EntityListeners(AuditingEntityListener.class)
public class UserEntity {

  @Id
  @GeneratedValue(generator="system-uuid")
  @GenericGenerator(name="system-uuid", strategy = "uuid2")
  private UUID id;

  @Column(name = "nickname")
  private String nickname;
  @Column(name = "password")
  private String password;
  @Column(name = "first_name")
  private String firstName;
  @Column(name = "last_name")
  private String lastName;
  @Column(name = "email")
  private String email;
  @Column(name = "avatar")
  private String avatar;
  @CreatedDate
  @Column(name = "date_of_registration")
  private LocalDate dateOfRegistration;
  @LastModifiedDate
  @Column(name = "last_update_date")
  private LocalDate lastUpdateDate;
  @LastModifiedBy
  @Column(name = "update_by")
  private String updateBy;
  @Column(name = "delete_date")
  private LocalDate deleteDate;
  @Column(name = "delete_by")
  private String deleteBy;
  @Column(name = "admin_panel_access")
  private boolean adminPanelAccess;

  @ManyToMany(mappedBy = "users")
  private Set<CourseEntity> courses;

}