package com.mts.teta.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.mts.teta.model.Message;
import com.mts.teta.repository.EnrichmentRepository;
import com.mts.teta.repository.MessageRepository;
import com.mts.teta.repository.impl.EnrichmentRepositoryImpl;
import com.mts.teta.repository.impl.MessageRepositoryImpl;
import com.mts.teta.service.impl.EnrichmentServiceImpl;
import com.mts.teta.service.impl.PhoneMessageEnrichment;
import com.mts.teta.utils.TestUtils;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EnrichmentServiceTest {

  private final static int THREAD_NUMBER = 100;

  private final MessageRepository messageRepository = new MessageRepositoryImpl();
  private final EnrichmentRepository enrichmentRepository = new EnrichmentRepositoryImpl();
  private final PhoneMessageEnrichment enrichmentFactory = new PhoneMessageEnrichment(messageRepository, enrichmentRepository);
  private final EnrichmentService enrichmentService = new EnrichmentServiceImpl(enrichmentFactory);

  @Test
  void enrich_testSuccessEnrichment_statusOkReturned() throws InterruptedException {
    messageRepository.deleteAll();
    ExecutorService service = Executors.newFixedThreadPool(10);
    CountDownLatch latch = new CountDownLatch(THREAD_NUMBER);
    for (int i = 0; i < THREAD_NUMBER; i++) {
      service.execute(() -> {
        Message fullEnrichmentMessage = TestUtils.getMessageWithFullContent();
        Message partEnrichmentMessage = TestUtils.getMessageWithContentWithoutEnrichment();
        Message unenrichmentMessage = TestUtils.getMessageWithNonJsonContent();

        enrichmentService.enrich(unenrichmentMessage);
        enrichmentService.enrich(fullEnrichmentMessage);
        enrichmentService.enrich(partEnrichmentMessage);
        latch.countDown();
      });
    }
    latch.await();

    List<String> enrichedList = messageRepository.findAllEnrichedMessage();
    List<String> unenrichedList = messageRepository.findAllUnenrichedMessage();

    Assertions.assertAll(
        () -> assertEquals(THREAD_NUMBER * 2, enrichedList.size()),
        () -> assertEquals(THREAD_NUMBER, unenrichedList.size())
    );

    assertEquals(THREAD_NUMBER, enrichedList.stream().filter(a -> a.contains(TestUtils.ENRICHED_PHONE)).count());
    assertEquals(THREAD_NUMBER, enrichedList.stream().filter(a -> a.contains(TestUtils.UNENRICHED_PHONE)).count());
  }

  @Test
  void enrich_sendNonJsonContent_unenrichedContentReturned() {
    messageRepository.deleteAll();
    enrichmentService.enrich(TestUtils.getMessageWithNonJsonContent());
    List<String> unenrichedList = messageRepository.findAllUnenrichedMessage();
    assertEquals(1, unenrichedList.size());
  }

  @Test
  void enrich_sendContentWithEmptyPhone_unenrichedContentReturned() {
    messageRepository.deleteAll();
    enrichmentService.enrich(TestUtils.getMessageWithContentWithEmptyPhone());
    List<String> unenrichedList = messageRepository.findAllUnenrichedMessage();
    assertEquals(1, unenrichedList.size());
  }

  @Test
  void enrich_sendContentWithoutPhone_unenrichedContentReturned() {
    messageRepository.deleteAll();
    enrichmentService.enrich(TestUtils.getMessageWithContentWithoutPhone());
    List<String> unenrichedList = messageRepository.findAllUnenrichedMessage();
    assertEquals(1, unenrichedList.size());
  }

  @Test
  void enrich_sendContentWithEnrichment_updatedContentReturned() {
    messageRepository.deleteAll();
    Message message = TestUtils.getMessageWithFullContent();
    assertTrue(message.getContent().contains("Vasya"));
    assertTrue(message.getContent().contains("Ivanov"));

    String content = enrichmentService.enrich(TestUtils.getMessageWithFullContent());
    List<String> enrichedList = messageRepository.findAllEnrichedMessage();
    List<String> unenrichedList = messageRepository.findAllUnenrichedMessage();
    assertEquals(1, enrichedList.size());
    assertEquals(0, unenrichedList.size());

    JSONObject enrichment = new JSONObject(content).getJSONObject("enrichment");

    assertEquals("Kurt", enrichment.get("firstName"));
    assertEquals("Vonnegut", enrichment.get("lastName"));
  }

  @Test
  void enrich_sendContentWithWrongNumber_unenrichedContentReturned() {
    messageRepository.deleteAll();
    enrichmentService.enrich(TestUtils.getMessageWithFullContentAndWrongPhone());
    List<String> unenrichedList = messageRepository.findAllUnenrichedMessage();
    assertEquals(1, unenrichedList.size());
  }

  @Test
  void enrich_validateResult_correctJsonReturned() {
    messageRepository.deleteAll();
    String content = enrichmentService.enrich(TestUtils.getMessageWithContentWithoutEnrichment());
    JSONObject jsonContent = new JSONObject(content);

    assertNotNull(jsonContent.get("java"));
    assertNotNull(jsonContent.get("test"));
    assertNotNull(jsonContent.get("msisdn"));

    JSONObject jsonEnrichment = jsonContent.getJSONObject("enrichment");

    assertEquals("Barbara", jsonEnrichment.get("firstName"));
    assertEquals("Liskov", jsonEnrichment.get("lastName"));
  }
}