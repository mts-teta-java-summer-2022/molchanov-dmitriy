package com.mts.teta.utils;

import com.mts.teta.model.Message;
import com.mts.teta.model.Message.EnrichmentType;

public class TestUtils {

  public final static String ENRICHED_PHONE = "88005553536";
  public final static String UNENRICHED_PHONE = "88005553537";

  public static Message getMessageWithNonJsonContent() {
    return Message
        .builder()
        .content("not json content")
        .enrichmentType(EnrichmentType.MSISDN)
        .build();
  }

  public static Message getMessageWithContentWithoutPhone() {
    return Message
        .builder()
        .content("{\"action\" : \"action\", \"page\" : \"page\"}")
        .enrichmentType(EnrichmentType.MSISDN)
        .build();
  }

  public static Message getMessageWithContentWithEmptyPhone() {
    return Message
        .builder()
        .content("{\"action\" : \"action\", \"page\" : \"page\", \"msisdn\" : \"\"}")
        .enrichmentType(EnrichmentType.MSISDN)
        .build();
  }

  public static Message getMessageWithContentWithoutEnrichment() {
    return Message
        .builder()
        .content("{\"msisdn\" : \"88005553536\", \"test\" : \"test\", \"java\" : \"kotlin\"}")
        .enrichmentType(EnrichmentType.MSISDN)
        .build();
  }

  public static Message getMessageWithFullContent() {
    return Message
        .builder()
        .content("{\n"
            + "    \"test\": \"test\",\n"
            + "    \"java\": \"kotlin\",\n"
            + "    \"msisdn\": \"88005553537\",\n"
            + "    \"enrichment\": {\n"
            + "        \"firstName\": \"Vasya\",\n"
            + "        \"lastName\": \"Ivanov\"\n"
            + "    }\n"
            + "}\n")
        .enrichmentType(EnrichmentType.MSISDN)
        .build();
  }

  public static Message getMessageWithFullContentAndWrongPhone() {
    return Message
        .builder()
        .content("{\n"
            + "    \"test\": \"test\",\n"
            + "    \"java\": \"kotlin\",\n"
            + "    \"msisdn\": \"80000000000\""
            + "}\n")
        .enrichmentType(EnrichmentType.MSISDN)
        .build();
  }

}