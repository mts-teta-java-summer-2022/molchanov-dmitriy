package com.mts.teta.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.mts.teta.exception.UserNotFoundException;
import com.mts.teta.model.PhoneEnrichment;
import com.mts.teta.repository.impl.EnrichmentRepositoryImpl;
import com.mts.teta.utils.TestUtils;
import org.junit.jupiter.api.Test;

public class EnrichmentRepositoryTest {

  EnrichmentRepository enrichmentRepository = new EnrichmentRepositoryImpl();

  private static final String INVALID_PHONE = "9900000000";

  @Test
  void getEnrichmentByPhone_getByValidPhone_correctEnrichmentReturned() {
    PhoneEnrichment enrichment = enrichmentRepository.getEnrichmentByPhone(TestUtils.UNENRICHED_PHONE);
    assertEquals("Kurt", enrichment.getFirstName());
    assertEquals("Vonnegut", enrichment.getLastName());
  }

  @Test
  void getEnrichmentByPhone_getByInvalidPhone_exceptionReturned() {
    assertThrows(UserNotFoundException.class,
        () -> enrichmentRepository.getEnrichmentByPhone(INVALID_PHONE),
        String.format("user with phone %s not found", INVALID_PHONE));
  }

}