package com.mts.teta.exception;

public class UserNotFoundException extends RuntimeException {

  public UserNotFoundException(String phone) {
    super(String.format("user with phone %s not found", phone));
  }
}
