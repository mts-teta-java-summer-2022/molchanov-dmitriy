package com.mts.teta.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as=PhoneEnrichment.class)
public abstract class Enrichment {
}