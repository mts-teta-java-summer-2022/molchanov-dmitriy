package com.mts.teta.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Message {
  private String content;
  private EnrichmentType enrichmentType;

  public enum EnrichmentType {
    MSISDN;
  }
}
