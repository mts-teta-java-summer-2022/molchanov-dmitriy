package com.mts.teta.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.json.JSONObject;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PhoneEnrichment extends Enrichment {

  private String firstName;
  private String lastName;

  public JSONObject createJsonEnrichment() {
    JSONObject enrichmentJson = new JSONObject();
    enrichmentJson.put("firstName", firstName);
    enrichmentJson.put("lastName", lastName);
    return enrichmentJson;
  }
}
