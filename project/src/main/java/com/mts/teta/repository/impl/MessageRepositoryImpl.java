package com.mts.teta.repository.impl;

import com.mts.teta.repository.MessageRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MessageRepositoryImpl implements MessageRepository {

  private static final List<String> enrichedMessagesList = Collections.synchronizedList(new ArrayList<>());
  private static final List<String> unenrichedMessagesList = Collections.synchronizedList(new ArrayList<>());

  @Override
  public void save(String content, boolean isEnriched) {
    if(isEnriched)
      enrichedMessagesList.add(content);
    else
      unenrichedMessagesList.add(content);
  }

  @Override
  public synchronized List<String> findAllEnrichedMessage() {
    return enrichedMessagesList;
  }

  @Override
  public synchronized List<String> findAllUnenrichedMessage() {
    return unenrichedMessagesList;
  }

  @Override
  public void deleteAll() {
    enrichedMessagesList.clear();
    unenrichedMessagesList.clear();
  }

}