package com.mts.teta.repository;

import java.util.List;

public interface MessageRepository {

  void save(String content, boolean isEnriched);

  List<String> findAllEnrichedMessage();

  List<String> findAllUnenrichedMessage();

  void deleteAll();

}