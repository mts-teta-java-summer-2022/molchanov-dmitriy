package com.mts.teta.repository;

import com.mts.teta.model.PhoneEnrichment;

public interface EnrichmentRepository {

  PhoneEnrichment getEnrichmentByPhone(String phone);

}