package com.mts.teta.repository.impl;

import com.mts.teta.exception.UserNotFoundException;
import com.mts.teta.model.PhoneEnrichment;
import com.mts.teta.repository.EnrichmentRepository;
import java.util.Map;

public class EnrichmentRepositoryImpl implements EnrichmentRepository {

  private final Map<String, PhoneEnrichment> users = Map.of(
      "88005553535", PhoneEnrichment.builder().firstName("John").lastName("Doe").build(),
      "88005553536", PhoneEnrichment.builder().firstName("Barbara").lastName("Liskov").build(),
      "88005553537", PhoneEnrichment.builder().firstName("Kurt").lastName("Vonnegut").build(),
      "88005553538", PhoneEnrichment.builder().firstName("Charles").lastName("Bukowski").build(),
      "88005553539", PhoneEnrichment.builder().firstName("Richard").lastName("Feynman").build()
  );

  @Override
  public PhoneEnrichment getEnrichmentByPhone(String phone) {
    PhoneEnrichment enrichment = users.get(phone);
    if (enrichment != null)
      return enrichment;
    throw new UserNotFoundException(phone);
  }

}