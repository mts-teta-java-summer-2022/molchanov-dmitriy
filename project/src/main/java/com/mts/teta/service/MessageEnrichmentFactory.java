package com.mts.teta.service;

import com.mts.teta.model.Message;

public interface MessageEnrichmentFactory {

  String enrich(Message message);

}
