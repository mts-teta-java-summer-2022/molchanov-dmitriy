package com.mts.teta.service.impl;

import com.mts.teta.model.Message;
import com.mts.teta.model.Message.EnrichmentType;
import com.mts.teta.service.EnrichmentService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EnrichmentServiceImpl implements EnrichmentService {

  private final PhoneMessageEnrichment phoneMessageEnrichment;

  @Override
  public String enrich(Message message) {
    String content = message.getContent();
    if (message.getEnrichmentType() == EnrichmentType.MSISDN) {
      content = phoneMessageEnrichment.enrich(message);
    }
    return content;
  }
}
