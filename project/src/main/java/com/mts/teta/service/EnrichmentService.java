package com.mts.teta.service;

import com.mts.teta.model.Message;

public interface EnrichmentService {

  String enrich(Message message);

}