-- Количество пользователей, которые не создали ни одного поста.
select count(*) from profile p
where p.profile_id
not in (select profile_id from post);
-- 5

-- ID всех постов по возрастанию, у которых 2 комментария, title начинается с цифры, а длина content больше 20
select p.post_id from post p
join comment c on p.post_id = c.post_id
where length(p.content) > 20 and p.title ~ '^\d+'
group by p.post_id
having count(c.comment_id) = 2
order by p.post_id;
-- 22
-- 24
-- 26
-- 28
-- 32
-- 34
-- 36
-- 38
-- 42
-- 44

-- первые 3 ID постов по возрастанию, у которых либо нет комментариев, либо он один
select p.post_id from post p
join comment c on p.post_id = c.post_id
group by p.post_id
having count(c.post_id) < 2
order by p.post_id
limit 3;
-- 1
-- 3
-- 5